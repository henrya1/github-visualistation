# Github Social Graph
Assingment for CSU33012 - Software Engineering

This project is a continuation of a previous assignment -  [Interrogating the Github API](https://bitbucket.org/henrya1/interrogating-the-github-api/src/master/)


## Instructions to Build and Run Locally
+   **Make sure you have** [Python 3.7+](https://www.python.org/downloads/)
+   Clone or download the repo: `git clone https://henrya1@bitbucket.org/henrya1/github-visualistation.git`
+	Download the commits_info.csv from  [here](https://bitbucket.org/henrya1/github-visualistation/downloads/)
+   Navigate to the repo: `cd ../../github-visualisation`
+ 	Put commits_info.csv into this directory
+   Install Dependancies: `pip install -r requirements.txt`
+   Make sure you are in  `github-social-graph/social-graph`
+   Run `python app.py`
+   Navigate to link from terminal or: [http://127.0.0.1:8050/](http://127.0.0.1:8050/)


## Features

### A Graph showing Commits
+   Shows commits across all *aseprite's* public repositories.
+   Ability to zoom and pan
+   Save graph as PNG, ability to save zoomed in state
+	The pink of the bars gets darker with larger amounts of commits

### Languages Piechart
+   Shows the languages used by Asperite in their repository's and their occurences.
+   Ability to save as PNG
+   Include/Exclude languages shown by clicking entry in legend.
+   Double click entry to show it exclusively.
+   Double click again to revert back to default state.

