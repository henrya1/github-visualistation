import requests
import plotly.express as px
import pandas as pd


class Project:
    def __init__(self, id, name, desc, created_at, updated_at, owner, open_issues_count, url, commits_url,
                 language_url):
        self.id = id
        self.name = name
        self.desc = desc
        self.created_at = created_at
        self.updated_at = updated_at
        self.owner = owner
        self.open_issues_count = open_issues_count
        self.url = url
        self.commits_url = commits_url
        self.language_url = language_url

    def print_project(self):
        print(
            'Name: ' + self.name + ' ID: ' + str(self.id) + ' Desc: ' + 'Languages URL: '+ self.language_url)
        print('\n')

    def get_list(self):
        data = [self.id, self.name, self.desc, self.created_at, self.updated_at, self.owner, self.open_issues_count,
                self.url, self.commits_url, self.language_url]
        return data


##### Main ####

username = input("Enter the github username:")
token = input("Enter the access token:")
url = 'https://api.github.com/users/aseprite/repos'

print('processing repos....')
repos = []
page_no = 1
while True:
    response = requests.get(url + '?page=' + str(page_no), auth=(username, token))
    json = response.json()
    for project in json:
        repo = Project(project['id'], project['name'], project['name'], project['id'], project['created_at'],
                       project['updated_at'], project['owner']['login'], project['url'],
                       project['commits_url'].split("{")[0], project['url']+'/languages')
        repo.print_project();
        repos.append(repo)
    if len(json) == 30:
        page_no = page_no + 1
    else:
        break

repo_info = []
for repo in repos:
    repo_info.append(repo.get_list())

repo_dataframe = pd.DataFrame(repo_info, columns=['Id', 'Name', 'Description', 'Created on', 'Updated on',
                                                  'Owner', 'Issues count',
                                                  'Repo URL', 'Commits URL', 'Languages URL'])

for i in range(repo_dataframe.shape[0]):
    response = requests.get(repo_dataframe.loc[i, 'Languages URL'], auth=(username, token))
    json = response.json()
    if response != {}:
        languages = []
        for key, values in json.items():
            languages.append(key)
        languages = ', '.join(languages)
        repo_dataframe.loc[i, 'Languages'] = languages
    else:
        repo_dataframe.loc[i, 'Languages'] = ""

repo_dataframe.to_csv('repos_info.csv', index=False)
print('finished processing repos!\n')

print('processing commits....')
commits_info = []
for i in range(repo_dataframe.shape[0]):
    url = repo_dataframe.loc[i, 'Commits URL']
    params = {'page': 1, 'per_page': 100}
    r = requests.get(url, params= params, auth=(username, token))
    json = r.json()
    while len(json) == 100:
        for commit in json:
            commit_data = [repo_dataframe.loc[i, 'Id'], repo_dataframe.loc[i, 'Name'], str(commit['sha']),
                           commit['commit']['committer']['date'], commit['commit']['committer']['name']]
            commits_info.append(commit_data)
        params['page'] = params['page'] + 1
        r = requests.get(url, params=params, auth=(username, token))
        json = r.json()
    if 'message' not in json:
        for commit in json:
            commit_data = [repo_dataframe.loc[i, 'Id'], repo_dataframe.loc[i, 'Name'], str(commit['sha']),
                           commit['commit']['committer']['date'], commit['commit']['committer']['name']]
            commits_info.append(commit_data)


commit_dataframe = pd.DataFrame(commits_info, columns=['Repo Id', 'Repo Name', 'Commit Id', 'Date', 'Committer'])
commit_dataframe.to_csv('commits_info.csv', index=False)
print('finished processing commits!\n')

exit()
