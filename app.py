# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import pandas as pd
import numpy as np
import base64

###################################  Logic Goes Here ###################################
repos = pd.read_csv('repos_info.csv')
commits = pd.read_csv('commits_info.csv')

list_of_languages = []
for languages in repos['Languages']:
    if type(languages) == str:
        for language in languages.split(','):
            list_of_languages.append(language.strip())

languages_count = pd.Series(list_of_languages).value_counts()

commit_count = pd.DataFrame(pd.merge(repos,
                                     commits,
                                     left_on='Id',
                                     right_on='Repo Id',
                                     how='left'
                                     ).groupby('Id').size().reset_index())

commit_count.columns = ['Id', 'Commits count']
repos = pd.merge(repos, commit_count, on='Id')

repos_names = repos['Name']
commits = repos['Commits count']

color = np.array(['rgb(255,255,255)'] * commits.shape[0])
color[commits >= 1] = 'D3B6DF'  # pale green
color[commits >= 200] = 'C98DE2'  # green
color[commits >= 500] = 'B755E0'  # olive green
color[commits >= 1000] = '#9A00D9'  # dark green

###################################  Logic Goes Here  ###################################


###################################  Custom Styles  ###################################
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

title = {
    "font-weight": "bold",
    "text-align": "center",
}

subheading = {
    'text-align': 'center',
    'font-style': 'italic'
}

row = {
    'text-align': 'center',
}

image = {
    'display': 'inline-block',
    'width': '100px',
    'height': '100px',
    'padding': '10px'
}

###################################  Custom Styles  ###################################


app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1("Github Social Graph", style=title),

    html.Div([
        html.Div([
            dcc.Graph(
                id='languages_donut',
                figure={
                    'data': [
                        go.Pie(
                            labels=list_of_languages,
                            values=languages_count,
                            hole=.3
                        )
                    ],
                    'layout': {
                        'title': 'Top Languages Used',
                        'paper_bgcolor': 'rgba(0,0,0,0)',
                        'plot_bgcolor': 'rgba(0,0,0,0)'
                    }
                }
            )
        ], className="six columns"),
        html.Div([
            html.Div([
                dcc.Graph(
                    id='commits_per_repo',
                    figure={
                        'data': [
                            {'x': repos_names, 'y': commits, 'type': 'bar', 'marker': dict(color=color.tolist())}
                        ],
                        'layout': {
                            'title': 'Commits per Repository',
                            'paper_bgcolor': 'rgba(0,0,0,0)',
                            'plot_bgcolor': 'rgba(0,0,0,0)',
                            'xaxis': {
                                'title': {
                                    'text': 'Repository'
                                }
                            },
                            'yaxis': {
                                'title': {
                                    'text': 'Commit Count'
                                }
                            }
                        }
                    }
                )
            ], className="six columns"),
    ], className="row")
])])

if __name__ == '__main__':
    app.run_server(debug=True)
